<?php
//Template Name: Minha Conta
?>

<?php get_header(); ?>

<div class="second_header">
    <ul class="classe_lista">
        <li>Painel</li>
        <li>Pedidos</li>
        <li>Endereços</li>
        <li>Sair</li>
    </ul>
</div>

<h1></h1>
<?php if(have_posts()) {
        while(have_posts()) {
            the_post();
            
            the_content();        
        }
    }
    ?>

<?php get_footer(); ?>