<?php
function show_products($products){
    ?>
    <ul class="products_list">

        <?php foreach($products as $product){?>

        <li class="item_product">
            <div class="gototop">
            <a class="gototop" href="<?php echo $product->get_permalink(); ?>">
        </div>
                <?= $product->get_image();?>                
                <div class="container_info_products">
                    <h2>
                        <?= $product->get_name(); ?>
                    </h2>
                    <p class="products_price">
                        <?= $product->get_price_html();?>
                    </p>
                    <div class="adicionar_carrinho">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/Images/icones/Adicionar-ao-carrinho.png" alt="Carrinho">
                    </div>
                </div>                               
            </a>
        </li>

        <?php } ?>

    </ul>
    
    <?php   
}
?>

<?php 
    function options_of_the_week($amount, $dayOfWeek){
        $args = [
            'limit' => $amount,
            'tag' => [$dayOfWeek],
        ];
        return wc_get_products( $args );
    }
?>