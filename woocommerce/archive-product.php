<?php get_header(); ?>
<main class="main_content">
    <div class="menu_container">
        <div class="div_title_ProductList">
            <h1 class="title_ProductList">SELECIONE UMA CATEGORIA</h1>
        </div>
        <div class="categories_container">
            <?php wp_nav_menu(['menu' => 'categorias']); ?>
        </div>
    </div>
    
    <section class="sectionPlates">
        <h1 class="">PRATOS</h1>
        <h4 class="selectedCategory" hidden>[categoria selecionada]</h4>

    <!-- FILTROS BUSCA POR PRATOS -->
        <div class="filters">
            <div class="searchAndOrder">
                <label for="">Buscar por nome:</label>
                <?php 
                    get_product_search_form(); 
                    woocommerce_catalog_ordering();                    
                ?>
            </div>
            <div class="FilterByPrice">
                <label for="">Filtro de preço:</label><br>
                <label for="">De:</label>
                <input type="text">
                <label for="">Até:</label>
                <input type="text">
            </div>
        </div>
        
    </section>    

    <!-- EXIBIÇÃO DE PRODUTOS -->
    <section class="productsSection">
        
        <?php 
            $args = [        
                'orderby'=> 'date',
                'order' => 'DESC'
            ];

            $products = wc_get_products( $args );
            show_products($products);
            // echo get_the_posts_pagination( );
        ?> 
        
    </section>
    <div class="pagination">
        <?php echo get_the_posts_pagination( ); ?>
    </div>    
</main>

<script >
    
let buttonCategory1 = document.querySelector('.btn_category_1');
let buttonCategory2 = document.querySelector('.btn_category_2');
let buttonCategory3 = document.querySelector('.btn_category_3');
let buttonCategory4 = document.querySelector('.btn_category_4');

let textCategory = document.querySelector('.selectedCategory');
let clicked = false

buttonCategory1.addEventListener('click', () => {                                   
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "MASSAS"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "MASSAS"
        clicked = true
    }
})
buttonCategory2.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "NORDESTINA"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "NORDESTINA"
        clicked = true
    }
    
})
buttonCategory3.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "VEGETARIANA"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "VEGETARIANA"
        clicked = true
    }
    
})
buttonCategory4.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "JAPONESAS"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "JAPONESAS"
        clicked = true
    }
})
</script>

<?php get_footer(); ?>