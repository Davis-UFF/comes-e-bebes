<?php
// Template Name: Front-page

?>
<?php get_header(); ?>


<section class="header_home">
  <div class="info_header">
    <h1 class="info_header_title">
      Comes&Bebes
    </h1>
    <p class="info_header_text">
      O restaurante para todas as fomes
    </p>
  </div>
</section>

<section class="conheca-container">
  <div class="conheca">

    <h1 class="conheca_title_text">
      Conheça Nossa Loja
    </h1>

    <?php //get_product_search_form(); ?>
    
    <p class="conheca_tipos_pratos_text">
      Tipos de pratos principais
    </p>
    
    <?php 
        if(have_posts()) {
          while(have_posts()) {
              the_post();          
              the_content();
          }
        }
    
        $args = [
          'limit' => 4,
          'orderby'=> 'date',
          'order' => 'DESC'
        ];
    
      // Obtendo o dia da semana
      $dayOfWeek = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');
      $data = date('Y-m-d');
      $numberDayOfWeek = date('w', strtotime($data)); 

      ?>
      
      <p class="conheca_pratos_dia_text">
        Pratos do dia de hoje:<br>
        <span class="conheca_dia_semana">
          <?php print_r($dayOfWeek[$numberDayOfWeek]);?>
        <span>
        <?php           
        // Obtendo e exibindo o dia da semana
        $products = options_of_the_week(4, $dayOfWeek[$numberDayOfWeek]);  
        show_products($products);
        ?>        
      </p>      
    
      <div class="div_button">
        <a class="veja_opcoes_btn" href="http:/comes-e-bebes.local/loja/">Veja outras opções</a>
      </div>
  </div>
</section>

  <section class="footer_home">
    <div class="title_footer">
      <h2 class="title_footer_text">
        Visite Nossa Loja Física
      </h2>
    </div>
    
    <div class="info">

      <div class="adress">
        
        <div class="map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.683878061789!2d-43.13542792544125!3d-22.906378053987552!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e1!3m2!1spt-BR!2sbr!4v1660769541123!5m2!1spt-BR!2sbr" width="345" height="203" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade">
          </iframe>
        </div>
  
        <div class="adressText">
          <p>Av. Gal. Milton Tavares de Souza,s/n, São Domingos</p>
        </div>
        
        <div class="phoneText">
          <p>(21) 99999-9999</p>
        </div>
  
      </div>
      <div class="imageFooter">
        <img src="<?php echo get_stylesheet_directory_uri() ?>/Images/icones/Footer-amigos-comendo-juntos.png" alt="">      
      </div>

    </div>

  </section>

<?php get_footer(); ?>