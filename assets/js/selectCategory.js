
let buttonCategory1 = document.querySelector('.btn_category_1');
let buttonCategory2 = document.querySelector('.btn_category_2');
let buttonCategory3 = document.querySelector('.btn_category_3');
let buttonCategory4 = document.querySelector('.btn_category_4');

let textCategory = document.querySelector('.selectedCategory');
let clicked = false

buttonCategory1.addEventListener('click', () => {                                   
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "MASSAS"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "MASSAS"
        clicked = true
    }
})
buttonCategory2.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "NORDESTINA"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "NORDESTINA"
        clicked = true
    }
    
})
buttonCategory3.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "VEGETARIANA"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "VEGETARIANA"
        clicked = true
    }
    
})
buttonCategory4.addEventListener('click', () => {  
    if (clicked == true) {
        textCategory.toggleAttribute('hidden')
        textCategory.innerHTML = "JAPONESAS"
    }
    else{
        textCategory.removeAttribute('hidden')
        textCategory.innerHTML = "JAPONESAS"
        clicked = true
    }
})