<?php

function meu_estilo_css(){
    add_theme_support( 'woocommerce' );
    add_theme_support('Comes e Bebes');
}
add_action('after_setup_theme','meu_estilo_css');

function C_e_B_css(){
    wp_register_style('Comes e Bebes', get_template_directory_uri().'/'.'style.css',[], '1.0.0');
    wp_enqueue_style('Comes e Bebes', get_template_directory_uri().'/'.'style.css',[], '1.0.0');
}
add_action('wp_enqueue_scripts', 'C_e_B_css');

register_nav_menu( 'wp_nav_menu' , 'Menu de Navegação' );

// AÇÃO DE MENU DE CATEGORIAS PARA O WORDPRESS
function register_menu_categories(){
    register_nav_menu('menu-categorias', 'Menu de Categorias');
}
add_action('init', 'register_menu_categories');

// EXIBIÇÃO DAS CATEGORIAS 
function show_menu($itens){
    $i = 1;
    foreach($itens as $item){
        // Obtenção dos dados de cada categoria
        $category = get_term_by( 'term_id', $item->object_id, 'product_cat');    

        $thumbnail_id = get_term_meta($item->object_id, 'thumbnail_id', true);      
        // Caputura da imagem da categoria
        $image = wp_get_attachment_image($thumbnail_id);
        $image_url = wp_get_attachment_url($thumbnail_id);
        // Variação de classes para o botão
        $classButton = "btn_category_". $i;
        ?>     
        <!-- Exibição html da categoria -->
        <?php $linkCategory = get_term_link($category);?>
        <div class="category_content"> 
            <button class="<?= $classButton ?>">
            <?= $image ?>    
                          
            <a href="<?= $linkCategory ?>">
                <div class="title_category_content">                                     
                    <p>
                        <?= $item->title ?>
                    </p>
                </div>
            </a>   
            </button>
        </div>
       <?php  
       $i++; 
    }    
  }
  add_filter('wp_nav_menu_objects', 'show_menu');

// IMPORTAÇÃO DE ARQUIVOS JAVASCRIPT
function my_theme_scripts_function() {
    wp_enqueue_script( 'script', get_template_directory_uri() . 'assets/js/selectCategory.js');
}
add_action('wp_enqueue_scripts','my_theme_scripts_function');

require "inc/show_promotions.php";
require "inc/pagination.php";

// LIMITAÇÃO DE POSTS POR PÁGINAS
function CeB_loop_shop_per_page(){
    return 8;
}
add_filter( 'loop_shop_per_page', 'CeB_loop_shop_per_page' );

?>