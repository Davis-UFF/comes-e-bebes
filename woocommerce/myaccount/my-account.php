<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>


<div class="second_header">
    <ul class="classe_lista">
        <li class="classe_lista_li"><a href="http://comes-e-bebes.local/minha-conta/editar-conta/">Painel</a></li>
        <li class="classe_lista_li"><a href="http://comes-e-bebes.local/minha-conta/pedidos/">Pedidos</a></li>
        <li class="classe_lista_li"><a href="http://comes-e-bebes.local/minha-conta/editar-endereco/">Endereços</a></li>
        <li class="classe_lista_li"><a href="http://comes-e-bebes.local/minha-conta/sair/?_wpnonce=f0738eaec5">Sair</a></li>
    </ul>
</div>

<div class="woocommerce-MyAccount-content" style="float: none ; padding: 50px 200px; width: 100%;" >
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );
	?>
</div>
