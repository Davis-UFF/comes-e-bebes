<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> <?php bloginfo( 'name' ) ?> | <?php the_title(); ?> </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bellota+Text:wght@400;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<div class="container">
    <div class="header_div">
        <div class="logo">
            <a href="http://comes-e-bebes.local/"><img src="<?php echo get_stylesheet_directory_uri() ?>/Images/icones/logo.png" alt="Logo"></a>
            <div class="pesquisa">
               <?php get_product_search_form();?>
            </div>
        </div>
        <div class="icones">
            <a class="btn-pedido" href="http://comes-e-bebes.local/loja/">Faça um pedido</a>
            <div class="carimagem">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/Images/icones/Carrinho.png" alt="Carrinho">
            </div>
                
            <div class="menucarrinho">
                
                <div class="saindo">X</div>
                <div class="barra">
                    <h2 class="cartitle">CARRINHO</h2>
                </div>
                <div class="minicart">
                <?php
                    global $woocommerce;
                    $items = $woocommerce->cart->get_cart();
                    $cartmage = array(
                        'width'     => '110',   // px
                        'height'    => '100',   // px
                        'crop'      => 1        // true
                    );
                ?>  
                <div id="protucto">
                    <div id="produtofoto">
                        <?php foreach($items as $item => $values) { 
                            $_product =  wc_get_product( $values['data']->get_id() );
                            //product image
                            
                        }
                        ?>
                        </div>
                        <div id="produtoinfo">
                            
                        <?php foreach($items as $item => $values) {
                            ?>

                            <div id="geralcart">
                                <?php
                            $_product =  wc_get_product( $values['data']->get_id() );
                            $getProductDetail = wc_get_product( $values['product_id'] );
                            echo $getProductDetail->get_image($cartmage); // accepts 2 arguments ( size, attr )?>
                            <div id="infogeral">
                                <div id="protitulo">
                                <?php
                            echo $_product->get_title();
                                ?>
                                </div>
                            <div id="proquanti">
                            <?php
                            echo "Quantidade: ".$values['quantity'];
                            
                            ?>
                            </div>
                            <div id="infopreco">
                                <?php
                            $price = get_post_meta($values['product_id'] , '_price', true);
                            echo "  R$ ".$price."<br>";
                            ?>
                            </div>
                        </div>
                        


                            
                            
                            </div>
                            
                            <?php
                            /*Regular Price and Sale Price*/
                            
                        }
                        ?>
                        </div>
                </div>
                </div>
                <div class="barra"></div>
                <div id="totalprecoo">
                <?php
                echo "Total do Carrinho: ".WC()->cart->get_cart_subtotal()
                ?>
                </div>
                <div class="continuarcompra">
                <a class="continuarparacompra" href="http://comes-e-bebes.local/finalizar-compra/">
                    COMPRAR
                    </a>
                </div>
                
            </div>
            <script>
                const carimagem=document.querySelector('.carimagem')
                const menucarrinho=document.querySelector('.menucarrinho')
                const saindo=document.querySelector('.saindo')

                carimagem.onclick=function(){
                    menucarrinho.classList.toggle('open')
                    saindo.classList.toggle('aparecer')
                }
                saindo.onclick=function(){
                    menucarrinho.classList.remove('open')
                }
            </script>
            
            <a href="http://comes-e-bebes.local/minha-conta/editar-conta/"><img src="<?php echo get_stylesheet_directory_uri() ?>/Images/icones/user.png" alt="Usuário"></a>
        </div>

    </div>
</div>

<body <?php body_class(); ?>>
