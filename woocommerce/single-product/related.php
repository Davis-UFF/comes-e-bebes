<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) : ?>

	<section class="related products">
        

		<?php
        
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'Related products', 'woocommerce' ) );
       


        global $product;


        $id = $product->get_id();
        // echo($id);
        $category = $product->get_categories();
        // echo($category);
        // $catid= get_cat_ID( $category );
        // echo($catid);
        
        // $cat = $category[0]->term_id;
        // echo($cat);
        $catid= $product->category_ids[0];
        // echo($catid);
        // $catname=get_cat_name( int $cats);
        // echo($catname);
        // print_r(get_cat_name($catid));
       
        if( $term = get_term_by( 'id', $catid, 'product_cat' ) ){
            // echo $term->name;
            $catname=$term->name;
        }
        
        

		if ( $heading ) :
			?>
			<h2 class="maiscomida"><?php echo esc_html( 'Mais Comida'.' '.$catname ); ?></h2>
		<?php endif; ?>
        
		<div class="looprelativo">
		<?php woocommerce_product_loop_start(); ?>
			<div class="conteudosingle">
			<?php foreach ( $related_products as $related_product ) : ?>
					<div class="conteudosingle">
					<?php
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					?>
					
					<div class="conteudosingle"><?php wc_get_template_part( 'content', 'product' );?></div>
					</div>

			<?php endforeach; ?>
			</div>

		<?php woocommerce_product_loop_end(); ?>
		</div>

	</section>
	<?php
endif;

wp_reset_postdata();
