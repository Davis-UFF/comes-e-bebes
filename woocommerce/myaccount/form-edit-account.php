<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;



do_action( 'woocommerce_before_edit_account_form' ); ?>

<?php $usuario = get_current_user() ?>

<div class="form-container">


	<div class="edit_acount_usuario">
		<p class="nome_usuario">
			Olá, <strong><?php echo $usuario?></strong> (não é <strong><?php echo $usuario?></strong>? <a href="http://comes-e-bebes.local/minha-conta/sair/?_wpnonce=6f9c977c92">Sair</a>)
		</p>

		<p class="opcoes_usuario">
		<br>
		A partir do painel de controle de sua conta, você pode ver suas <a href="http://comes-e-bebes.local/minha-conta/pedidos/">compras recentes</a>, gerenciar seus <a href="http://comes-e-bebes.local/minha-conta/endereco/">endereços de entrega e faturamento</a>, e <a href="http://comes-e-bebes.local/minha-conta/editar-conta/">editar sua senha e detalhes da conta</a>.
		</p>
	</div>

	<div class="formulario">
			

		<form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
		
			<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
			
		
			<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first" id="formteste">
				<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required"></span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Digite seu nome" name="account_first_name" id="account_first_name" autocomplete="given-name" value="<?php echo esc_attr( $user->first_name ); ?>" />
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
				<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required"></span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" placeholder="Digite seu sobrenome" name="account_last_name" id="account_last_name" autocomplete="family-name" value="<?php echo esc_attr( $user->last_name ); ?>" />
			</p>
			<div class="clear"></div>
		
		
			<div class="clear"></div>
		
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required"></span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" placeholder="Digite seu email" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
			</p>
		
			
				<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>
		
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="Digite sua senha atual" name="password_current" id="password_current" autocomplete="off" />
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="Digite sua nova senha" name="password_1" id="password_1" autocomplete="off" />
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" placeholder="Confirme sua nova senha" name="password_2" id="password_2" autocomplete="off" />
				</p>
			
			<div class="clear"></div>
		
			<?php do_action( 'woocommerce_edit_account_form' ); ?>
		
			<p class="botaosalvar">
				<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
				<button type="submit" class="woocommerce-Button button" id="btnsalvar" name="save_account_details" value="<?php esc_attr_e( 'SALVAR ALTERAÇÕES', 'woocommerce' ); ?>"><?php esc_html_e( 'SALVAR ALTERAÇÕES', 'woocommerce' ); ?></button>
				<input type="hidden" name="action" value="save_account_details" />
			</p>
		
			<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
		</form>
		
		<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
	</div>


</div>